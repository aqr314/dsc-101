
<img src="https://iconape.com/wp-content/files/dk/191258/svg/191258.svg" alt=""
	title="logo" width="500" height="150" align="center" />

# Data Science 101 

This repository contains all the lectures, labs, planification ands all the material corresponding to the course **Introducción a la Ciencia de Datos para la Investigacion**, from the Computer Science School at Universidad Central de Venezuela.

## Content and Topics

- Topic 1: Introduction
- Topic 2: Getting and Preprocessing Data
- Topic 3: Data Analytics
- Topic 4: Machine Learning and Artificial Intelligence


## Evaluation

The course evaluation will be composed of 2 theoretical partial exams, and a practical project.

## Planification and Schedule

| Evaluation | Topic | Weight | Date  |
| ---------- | ----- | ----------- | ------ |
| Partial 1  | 1,2   | 35 %        | to be defined |
| Partial 2  | 3,4   | 35 %        | to be defined |
| Project    |       | 30 %        |to be defined |

## Teaching

- Alfredo Quintana, alfredo.quitana.14@gmail.com

## Books

- [Data Science, Kelleher](https://mitpress.mit.edu/books/data-science)
- [Data Mining, The Textbook, Aggarwal](https://link.springer.com/book/10.1007/978-3-319-14142-8)
- [Data Preprocessing in Data Mining](https://link.springer.com/book/10.1007/978-3-319-10247-4)
- [The Art of Data Science, R. Peng](https://www.amazon.es/Art-Data-Science-Roger-Peng/dp/1365061469)
- [What is Data Science?](https://www.amazon.es/What-Data-Science-Mike-Loukides-ebook/dp/B007R8BHAK/ref=sr_1_fkmrnull_5?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=what+is+data+science+oreilly&qid=1557374870&s=foreign-books&sr=1-5-fkmrnull)
- [Data Science for Business](https://www.amazon.es/Data-Science-Business-data-analytic-thinking/dp/1449361323/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=data+science+for+business&qid=1557374954&s=foreign-books&sr=1-1)
- [An Introduction to Statistical Learning](https://www.amazon.es/Introduction-Statistical-Learning-Applications-Statistics/dp/1461471370/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=introduction+to+statistical+learning&qid=1557374998&s=foreign-books&sr=1-1)
- [Big Data: Using Smart Big Data, Analytics and Metrics to Make Better Decisions and Improve Performance](https://www.amazon.es/Big-Data-Analytics-Decisions-Performance/dp/1118965833/ref=sr_1_13?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=big+data&qid=1557375139&s=foreign-books&sr=1-13)

## Other Courses

- [CS109 Data Science](http://cs109.github.io/2015/index.html)
- [CS229 Machine Learning](https://see.stanford.edu/course/cs229)
- [Introduction to Data Science](http://stellar.mit.edu/S/course/6/sp18/6.S077/)
- [DSC101 Umass Dartmouth](https://www.coursicle.com/umassd/courses/DSC/)

